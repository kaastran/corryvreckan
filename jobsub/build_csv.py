import json
import glob
import sys
import os
import logging
import misc
from argparse import ArgumentParser

def get_parser():
    parser = ArgumentParser()
    parser.add_argument("--template", action="store_true", help="Generate a template .json file as 'template.json' for use with this script, then exits.")
    parser.add_argument("-r", "--replacement-config", required="--template" not in sys.argv, help=".json file with basic replacement information.")
    parser.add_argument("-o", "--output", help="Output filename for csv.")
    parser.add_argument("--zfill", type=int, help="Number of zeroes to append in front of run numbers.")
    parser.add_argument("runs", nargs="*", help="Run number or range of run numbers (e.g. XXXX or XXXX-YYYY)")

    return parser
    
def dump_template_json():
    help_string = """
    This creates a template .json file. Fill this template to then generate a .csv file
    for running corryvreckan using jobsub.py. For each wildcard, build_csv.py will find
    a filename corresponding to each run number in the "path" provided for that wildcard,
    following the filename pattern provided in "pattern". Hashtag signs "#" are placeholders
    to show where the run number is in the data filenames. Make sure to use --zfill with the
    correct number of leading zeroes in the run numbers.
    
    template.json = {
        "Wildcard1" : {
                "path" : "/path/to/data/",
                "pattern" : "run######*.raw"
        },
        "Wildcard2" : {
                "path" : "/path/to/other/data/",
                "pattern" : "*run_######*.blck"
        }
    }
    
    Example:
    If the run number is 1234, using the above .json and option --zfill 2, build_csv.py will find a file in the
    folder given in "Wildcard1" : "path" matching the string "run001234*.raw".
    """
    print(help_string)
    template_dict = {
        "Wildcard1" : {
                "path" : "/path/to/data/",
                "pattern" : "run######*.raw"
        },
        "Wildcard2" : {
                "path" : "/path/to/other/data/",
                "pattern" : "*run_######*.blck"
        }
    }

    with open("template.json", "w") as fp:
        json.dump(template_dict, fp, indent=2)
    
    sys.exit(0)    
        

def get_filename_for_wildcard(wildcard_dict, run_number, zfill=None):
    path = wildcard_dict["path"]    
    pattern = wildcard_dict["pattern"]
    zfill_string = ""
    if zfill:
        for _ in range(zfill):
            zfill_string += "0"
    pattern = pattern[:pattern.find("#")] + zfill_string + run_number + pattern[pattern.rfind("#") + 1:]
    
    print(path)
    print(pattern)
    filename = glob.glob(os.sep.join([path, pattern]))
    if len(filename) > 1:
        sys.exit(f"Ambiguity - found several files for run {run_number}! Aborting.")
    if len(filename) == 0:
        sys.exit(f"Found no file for run {run_number}! Aborting.")
    filename = filename[0].split("/")[-1]
    return filename

def main():
    """
    Build a csv file with filenames of input data dynamically generated using run numbers,
    for replacing wildcards in corryvreckan configuration files.
    """
    args = get_parser().parse_args()
    
    if args.template:
        dump_template_json()
    
    with open(args.replacement_config, "r") as fp:
        replacement_dict = json.load(fp)
    
    log = logging.getLogger()
    
    runs = list()
    for runnum in args.runs:
        try:
            log.debug("Parsing run-range argument: '%s'", runnum)
            runs = runs + misc.parseIntegerString(runnum)
        except ValueError:
            log.error("The list of runs contains non-integer and non-range values: '%s'", runnum)
            return 2
    
    csv_string = "RunNumber,"
    for key in replacement_dict:
        csv_string += key + ","
    csv_string = csv_string[:-1]
    csv_string += "\n"
    
    for run in runs:
        csv_string += str(run) + ","
        for key in replacement_dict:
            filename = get_filename_for_wildcard(replacement_dict[key], str(run), args.zfill)
            csv_string += filename + ","
        csv_string = csv_string[:-1]
        csv_string += "\n"
    csv_string = csv_string[:-1]
    
    if args.output:
        with open(args.output, "w") as fp:
            fp.write(csv_string)
    else:
        with open("replacement_config.csv", "w") as fp:
            fp.write(csv_string)
            
    
if __name__ == "__main__":
    main()